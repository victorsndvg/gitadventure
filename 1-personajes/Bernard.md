# Personaje

|||
| --------------------- | ----------- |
| Avatar                | ![Minion](https://octodex.github.com/images/minion.png) |
| Nombre                | Rellene aquí el nombre. |
| Edad                  | Rellene aquí la edad.   |
| Fecha de nacimiento   | Rellene aquí la fecha.  |
| Lugar de nacimiento   | Rellene aquí el lugar.  |
| Otros                 | Rellene aquí libremente.|
