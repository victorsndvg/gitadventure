# Aventura Git

## Enunciado

<strong>Bernard</strong> tiene una importante misión. 
Sus dos amigos <strong>Hoagie</strong> y <strong>Laverne</strong> le acompañarán.
Deben regresar a la mansión de la familia Edison para llevarla a cabo. 

Oh no! 
Una de las dos creaciones del Dr. Fred Edison, 
el <strong>Tentáculo Púrpura</strong>, 
ha ingerido un residuo radioactivo de la Mansión con inquietantes consecuencias, 
su mente se ha visto perturbada ... 

y ahora se propone conquistar el Mundo!!!

Ante ese escenario, 
Bernard y sus amigos planean volver atrás en el tiempo 
para impedir que el Tentáculo Púrpura pueda beber esos residuos radioactivos.
Para ello pretenden utilizar la <strong>Máquina del tiempo</strong> del Dr. Fred. Bernard. 
Bernard, Hoagie y Laverne se reunen en la Máquina del tiempo para salvar al mundo. 

Lamentablemente, para viajar en el tiempo es necesario a una fuente de energía 
lo suficientemente potente como para poder regresar y vencer al tentáculo púrpura. 

Por desgracia, faltan tres componentes para poder utilizar la <strong>Super batería</strong> con la máquina del tiempo. 

Encuentra esos componentes, colócalos en la Super Batería y salva al mundo del Tentáculo Púrpura!

## Instrucciones

Para completar la misión:

- Al comenzar, cada equipo debe:

1. Duplicar este repositorio.
2. Repartir los personaje (Bernard, Hoagie y Laverne) entre los miembros del equipo.

- A continuación, cada miembro del equipo debe:

3. Obtener una copia local del repositorio duplicado.
4. Explorar el repositorio y seguir las instrucciones que se encuentre.

- Para finalizar, el equipo debe:

5. Asegurarse y revisar que se ha cumplido la misión correctamente.
6. Enviar el resultado.

## Info

Consulta la [Pista](2-pistas/pista-enunciado.md) para tener más detalles de las instrucciones.

Consulta la [Ayuda](3-ayuda/ayuda-enunciado.md) para tener más información sobre los comandos a utilizar.
