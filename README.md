# Aventura Git

## Enunciado

<table style="width:100%">
  <tr>
    <td>
<strong>Bernard</strong> tiene una importante misión. <br/>
Sus dos amigos <strong>Hoagie</strong> y <strong>Laverne</strong> le acompañarán. <br/>
Deben regresar a la mansión de la familia Edison para llevarla a cabo. <br/>
    </td>
    <td>
      <img src="0-media/Cabecera.png"  width="640" height="360">
    </td>
  </tr>
</table> 

<table style="width:100%">
  <tr>
    <td>
      <img src="0-media/Noticia.jpg"  width="640" height="320">
    </td>
    <td>
Oh no! <br/>
Una de las dos creaciones del Dr. Fred Edison, <br/>
el <strong>Tentáculo Púrpura</strong>, <br/>
ha ingerido un residuo radioactivo de la Mansión con inquietantes consecuencias, <br/>
su mente se ha visto perturbada ... <br/>
<br/>
<h3>y ahora se propone conquistar el Mundo!!!</h3>
    </td>
  </tr>
</table> 

<table style="width:100%">
  <tr>
    <td>
Ante ese escenario, <br/> 
Bernard y sus amigos planean volver atrás en el tiempo <br/> 
para impedir que el Tentáculo Púrpura pueda beber esos residuos radioactivos. <br/>
Para ello pretenden utilizar la <strong>Máquina del tiempo</strong> del Dr. Fred. Bernard. <br/>
Bernard, Hoagie y Laverne se reunen en la Máquina del tiempo para salvar al mundo. <br/>
<br/>
Lamentablemente, para viajar en el tiempo es necesario a una fuente de energía <br/> 
lo suficientemente potente como para poder regresar y vencer al tentáculo púrpura. <br/>
<br/>
Por desgracia, faltan tres componentes para poder utilizar la <strong>Super batería</strong> con la máquina del tiempo. <br/>
<br/>
<h3>Encuentra esos componentes, colócalos en la Super Batería y salva al mundo del Tentáculo Púrpura!</h3>
    </td>
    <td>
      <img src="0-media/SuperBateria.png"  width="640" height="340">
    </td>
  </tr>
</table> 



## Instrucciones

Para completar la misión:

- Al comenzar, cada equipo debe:

1. Duplicar este repositorio.
2. Repartir los personaje (Bernard, Hoagie y Laverne) entre los miembros del equipo.

- A continuación, cada miembro del equipo debe:

3. Obtener una copia local del repositorio duplicado.
4. Explorar el repositorio y seguir las instrucciones que se encuentre.

- Para finalizar, el equipo debe:

5. Asegurarse y revisar que se ha cumplido la misión correctamente.
6. Enviar el resultado.

## Info

Consulta la [Pista](2-pistas/pista-enunciado.md) para tener más detalles de las instrucciones.

Consulta la [Ayuda](3-ayuda/ayuda-enunciado.md) para tener más información sobre los comandos a utilizar.
