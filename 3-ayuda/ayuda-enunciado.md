# Ayuda

<img src="0-media/iconos/ayuda.png"  width="140" height="140">

## Fork

__Fork__ es una copia de un repositorio. Hacer __Fork__ te permite realizar cambios libremente en un repositorio sin llegar a afectar al repositorio original.

__Fork__ es una característica que ofrecen los servicios de repositorios remotos como GitHub y Gitlab. Para obtener más información sobre como utilizarlo, consulta los siguientes enlaces:

- [Gitlab](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
- [Github](https://help.github.com/en/articles/fork-a-repo)

## Clone

Utiliza el commando __clone__ de Git para obtener una copia local de el respositorio duplicado (__Fork__) de tu equipo.

La sintaxis que sigue el comando __clone__ es la siguiente:

```
$ git clone URL [DIRECTORIO]
```

En donde la `URL` es la dirección remota del repositorio y `DIRECTORIO` (opcional) la ruta donde guardar el repositorio localmente.

[Más información del comando clone en la guía oficial](https://git-scm.com/docs/git-clone)

## Add

Cuando realizas cambios sobre un fichero, si quieres marcar dichos cambios para añadirlos debes usar el commando __add__ de Git.

El comando __add__ de Git prepara y registra los cambios que has realizado para el próximo __commit__.

La sintaxis que sigue el comando __add__ es la siguiente:

```
$ git add FICHERO
```

En donde la `FICHERO` es archivo modificado que quieres marcar para añadir en un próximo commit.

[Más información del comando add en la guía oficial](https://git-scm.com/docs/git-add)

## Commit

Cuando tengas claro que quieres registrar los cambios añadidos al historial de tu repositorio, debes utilizar el commando __commit__ de Git.

Crear un nuevo __commit__ significa guardar y registrar un nuevo estado en tu repositorio.

La sintaxis que sigue el comando __commit__ es la siguiente:

```
$ git commit [opciones]
```

El commando __commit__ te pedirá que escribas un mensaje describiendo los cambios incorporados. Intenta ser descriptivo.

[Más información del comando commit en la guía oficial](https://git-scm.com/docs/git-commit)

## Merge

El comando __merge__ de Git incorpora cambios de una rama o commit particular en otra.

Utiliza el comando __merge__ para llevar los componentes encontrados en el commit actual a la rama adecuada (`maquinadeltiempo`).

La sintaxis que sigue el comando __merge__ es la siguiente:

```
$ git merge RAMA
```

En donde la `RAMA` es la rama destino en la que se añadirán los cambios desde el commit actual.

[Más información del comando merge en la guía oficial](https://git-scm.com/docs/git-merge)

## Pull

El comando __pull__ de Git obtiene el estado actual de una rama del repositorio remoto y los mezcla en la rama local actual.

Utiliza el comando __pull__ para obtener localmente los cambios que hayan publicado tus compañeros de equipo.

La sintaxis que sigue el comando __pull__ es la siguiente:

```
$ git pull origin RAMA
```

En donde la `RAMA` es la rama origen de la que se descargarán los cambios desde el repositorio remoto.

[Más información del comando pull en la guía oficial](https://git-scm.com/docs/git-pull)

## Push

El comando __push__ de Git publica el estado actual de una rama local en el repositorio remoto.

Utiliza el comando __push__ para compartir con tus compañeros de equipo los cambios que realices localmente.

La sintaxis que sigue el comando __push__ es la siguiente:

```
$ git push origin RAMA
```

En donde la `RAMA` es la rama destino en la que se publicarán los cambios en el repositorio remoto.

[Más información del comando pull en la guía oficial](https://git-scm.com/docs/git-pull)

## Branch

Las ramas (__branch__) de Git son nombres que referencian un determinado estado de un repositorio.

El commando __branch__ de Git te permite listar las distintas ramas de un determinado proyecto.

La sintaxis que sigue el comando __branch__ es la siguiente:

```
$ git branch [opciones]
```

[Más información del comando branch en la guía oficial](https://git-scm.com/docs/git-branch)

## Checkout

El commando __checkout__ de Git te permite explorar localmente un estado del repositorio.

Con __checkout__ puedes navegar por los __commits__ y las ramas (__branch__) del repositorio.

La sintaxis que sigue el comando __checkout__ es la siguiente:

```
$ git checkout REFERENCIA
```

En donde la `REFERENCIA` es el identificador de un commit o una rama.

[Más información del comando checkout en la guía oficial](https://git-scm.com/docs/git-checkout)

